import { RepositoryItem } from "./RepositoryItem";

const repository = {
  name: "scp",
  description: "Sistema de carência",
  link: "https://github.com/esbnet/scp",
};
export function RepositoryList() {
  return (
    <section className="repository-list">
      <h1>Lista de Repositórios</h1>

      <ul>
        <RepositoryItem repository = {repository} />
      </ul>
    </section>
  );
}
